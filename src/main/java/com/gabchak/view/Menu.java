/***
 * View.
 */
package com.gabchak.view;

import com.gabchak.controller.Controller;
import com.gabchak.controller.ControllerImpl;
import com.gabchak.user.Appliances.Appliance;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


/***
 * Class Menu.
 */
public class Menu {

    /***
     * Get input from the keyboard.
     */
    private static Scanner input = new Scanner(System.in);
    /***
     *Instance of Controller.
     */
    private Controller controller;
    /***
     *hashmap saves the keys and values.
     */
    private Map<String, String> menu;
    /***
     *hashmap saves the keys and action.
     */
    private Map<String, Printable> methodsMenu;

    /***
     * Menu.
     */
    public Menu() {

        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Show appliances list.");
        menu.put("2", "  2 - Show connected appliances.");
        menu.put("3", "  3 - Sort by title.");
        menu.put("4", "  4 - Sort by power consumption.");
        menu.put("5", "  5 - Search for appliances in the power range (Watt)");
        menu.put("6", "  6 - Show power consumption of connected appliances.");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    /***
     *The method checks whether the entered data is a number.
     * @return - return integer valuer.
     */
    private static int inputVerified() {
        int num = 0;
        while (!input.hasNextInt()) {
            System.out.println("This is not a number.");
            input.next();
        }
        num = input.nextInt();
        if (num < 0) {
            System.out.println("Wrong input.");
            inputVerified();
        }
        return num;
    }

    /***
     *Method print appliances list.
     * @param appliances - get as param appliances list.
     */
    private static void printList(final List<Appliance> appliances) {
        int num = 0;
        for (Appliance appliance : appliances) {
            System.out.println((++num) + ". " + appliance.toString());
        }
    }

    /***
     *When the input is 1,
     * method print appliances list.
     */
    private void pressButton1() {
        printList(controller.getApplianceList());
    }

    /***
     *When the input is 2,
     * method print connected appliances list.
     */
    private void pressButton2() {
        printList(controller.sortByConnectedAppliance(
                controller.getApplianceList()));
    }

    /***
     *When the input is 3,
     * method sort by title appliances list.
     */
    private void pressButton3() {
        controller.sortByTitle();
    }

    /***
     *When the input is 4,
     * method sort by power consumption appliances list.
     */
    private void pressButton4() {
        controller.sortByPowerConsumption();
    }
    //-------------------------------------------------------------------------

    /***
     *When the input is 5,
     * method print appliances in the power range (Watt)
     * form (input) to (input) range.
     */
    private void pressButton5() {
        System.out.println("from: ");
        int from = inputVerified();
        System.out.println("to: ");
        int to = inputVerified();
        printList(controller.appliancesSearch(from, to));
        input.nextLine();

    }

    /***
     *When the input is 6,
     * method print total consumption of attached devices.
     */
    private void pressButton6() {
        System.out.println("Total consumption of attached devices: ");
        System.out.println(controller.countPowerConsumption(
                controller.sortByConnectedAppliance(
                        controller.getApplianceList())) + " Watt");

    }

    /***
     * Method output Menu.
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * Method show menu and get input from the user.
     */
    public final void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = String.valueOf(inputVerified());
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}

