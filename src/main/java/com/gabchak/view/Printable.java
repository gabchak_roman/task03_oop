/***
 * View.
 */
package com.gabchak.view;

/**
 * This is FunctionalInterface.
 */
public interface Printable {

    /**
     * Method print to implement.
     */
    void print();
}
