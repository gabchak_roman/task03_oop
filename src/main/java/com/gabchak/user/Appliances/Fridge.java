package com.gabchak.user.Appliances;

/***
 *Class represent Fridge.
 */
public class Fridge extends Appliance {

    /***
     * @param watts - appliance consumption.
     * @param connectionStatus - appliance connection Status.
     */
    public Fridge(final int watts, final boolean connectionStatus) {
        super("Fridge", watts, connectionStatus);
    }
}
