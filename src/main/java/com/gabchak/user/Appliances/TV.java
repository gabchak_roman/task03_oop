package com.gabchak.user.Appliances;

/***
 *Class represent TV.
 */
public class TV extends Appliance {

    /***
     * @param watts - appliance consumption.
     * @param connectionStatus - appliance connection Status.
     */
    public TV(final int watts, final boolean connectionStatus) {
        super("TV", watts, connectionStatus);
    }
}
