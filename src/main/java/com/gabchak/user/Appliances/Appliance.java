package com.gabchak.user.Appliances;

/***
 *Parent abstract class for Appliances.
 */
public abstract class Appliance {

    /***
     * Appliance Type.
     */
    private String applianceType;

    /***
     *Electrical appliance consumption.
     */
    private int watts;

    /***
     *Appliance connection Status.
     */
    private boolean connectionStatus;

    /***
     *Appliance constructor.
     * @param appliance - appliance type.
     * @param wattsIn - appliance consumption.
     * @param connectionStatusL - appliance connection Status.
     */
    Appliance(final String appliance, final int wattsIn,
              final boolean connectionStatusL) {
        this.applianceType = appliance;
        this.watts = wattsIn;
        this.connectionStatus = connectionStatusL;
    }

    /***
     * @return - appliance type.
     */
    public final String getAppliance() {
        return applianceType;
    }

    /***
     *
     * @return - appliance consumption.
     */
    public final int getWatts() {
        return watts;
    }

    /***
     * @return - appliance connection Status.
     */
    public final boolean isConnectionStatus() {
        return connectionStatus;
    }

    /***
     * @return - appliance info.
     */
    @Override
    public final String toString() {
        String conStatus;
        if (this.connectionStatus) {
            conStatus = "connected";
        } else {
            conStatus = "not connected";
        }
        return this.applianceType
                + " = "
                + this.watts
                + "_W"
                + ",\n   Connection Status: "
                + conStatus;
    }
}

