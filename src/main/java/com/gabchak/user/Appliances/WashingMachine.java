package com.gabchak.user.Appliances;

/***
 *Class represent Washing Machine.
 */
public class WashingMachine extends Appliance {

    /***
     * @param watts - appliance consumption.
     * @param connectionStatus - appliance connection Status.
     */
    public WashingMachine(final int watts, final boolean connectionStatus) {
        super("Washing machine", watts, connectionStatus);
    }
}
