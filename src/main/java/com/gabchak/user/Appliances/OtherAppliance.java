package com.gabchak.user.Appliances;

/***
 *Class represent OtherAppliance.
 */
public class OtherAppliance extends Appliance {

    /***
     * @param appliance - appliance type;
     * @param watts - appliance consumption.
     * @param connectionStatus - appliance connection Status.
     */
    public OtherAppliance(final String appliance,
                          final int watts, final boolean connectionStatus) {
        super(appliance, watts, connectionStatus);
    }
}
