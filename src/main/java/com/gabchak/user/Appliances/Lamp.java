package com.gabchak.user.Appliances;

/***
 *Class represent Lamp.
 */
public class Lamp extends Appliance {

    /***
     * @param watts - appliance consumption.
     * @param connectionStatus - appliance connection Status.
     */
    public Lamp(final int watts, final boolean connectionStatus) {
        super("Lamp", watts, connectionStatus);
    }
}
