/***
 * User.Appliances
 */
package com.gabchak.user.Appliances;

/***
 *Class represent Air Conditioning.
 */
public class AirConditioning extends Appliance {

    /***
     * @param watts - appliance consumption.
     * @param connectionStatus - appliance connection Status.
     */
    public AirConditioning(final int watts, final boolean connectionStatus) {
        super("Air Conditioning", watts, connectionStatus);
    }
}
