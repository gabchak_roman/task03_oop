/***
 *User.
 */
package com.gabchak.user;

import com.gabchak.view.Menu;

/**
 * Class represent user application.
 *
 * @author Gabchak Roman.
 * @version 1.0.
 * @since 2019-11-12.
 */
public final class Application {
    /***
     * Hide constructor.
     */
    private Application() {
    }

    /**
     * Method main it is starting point for the program.
     *
     * @param args - parameters starting point.
     */
    public static void main(final String[] args) {
        Menu menu = new Menu();
        menu.show();
    }
}
