/***
 * Controller
 */
package com.gabchak.controller;

import com.gabchak.model.BusinessLogic;
import com.gabchak.model.Model;
import com.gabchak.user.Appliances.Appliance;

import java.util.List;

/***
 *Class ControllerImpl implements interface controller .
 */
public class ControllerImpl implements Controller {
    /***
     * Instance of Model.
     */
    private Model model;

    /**
     * Class constructor.
     */
    public ControllerImpl() {
        model = new BusinessLogic();
    }

    /***
     * @return - appliance List.
     */
    @Override
    public final List<Appliance> getApplianceList() {
        return model.getApplianceList();
    }

    /***
     *Sort appliance List by title.
     */
    @Override
    public final void sortByTitle() {
        model.sortByTitle();
    }

    /***
     *Sort appliance list by power consumption.
     */
    @Override
    public final void sortByPowerConsumption() {
        model.sortByPowerConsumption();
    }

    /***
     *Method return list of appliance in power consumption range form - to.
     * @param wattRangeFrom - power consumption range form.
     * @param wattRangeTo - power consumption range to.
     * @return -list of appliance in power consumption range form - to.
     */
    @Override
    public final List<Appliance> appliancesSearch(
            final int wattRangeFrom, final int wattRangeTo) {
        return model.appliancesSearch(wattRangeFrom, wattRangeTo);
    }

    /***
     *Appliance list is sorted by connected appliance.
     * @param list - appliances list.
     * @return - sorted by connected status appliance list.
     */
    @Override
    public final List<Appliance> sortByConnectedAppliance(
            final List<Appliance> list) {
        return model.sortByConnectedAppliance(list);
    }

    /***
     *Method count total appliances power consumption.
     * @param appliances - appliances list.
     * @return - total appliances power consumption.
     */
    @Override
    public final int countPowerConsumption(final List<Appliance> appliances) {
        return model.countPowerConsumption(
                model.sortByConnectedAppliance(
                        model.getApplianceList()));
    }
}
