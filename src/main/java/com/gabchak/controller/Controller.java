/***
 * Controller
 */
package com.gabchak.controller;

import com.gabchak.user.Appliances.Appliance;

import java.util.List;

/***
 *Controller interface.
 */
public interface Controller {

    /***
     * @return - appliance List.
     */
    List<Appliance> getApplianceList();

    /***
     *Sort appliance List by title.
     */
    void sortByTitle();

    /***
     *Sort appliance list by power consumption.
     */
    void sortByPowerConsumption();

    /***
     *Method return list of appliance in power consumption range form - to.
     * @param wattRangeFrom - power consumption range form.
     * @param wattRangeTo - power consumption range to.
     * @return -list of appliance in power consumption range form - to.
     */
    List<Appliance> appliancesSearch(int wattRangeFrom, int wattRangeTo);

    /***
     *Appliance list is sorted by connected appliance.
     * @param list - appliances list.
     * @return - sorted by connected status appliance list.
     */
    List<Appliance> sortByConnectedAppliance(List<Appliance> list);

    /***
     *Method count total appliances power consumption.
     * @param appliances - appliances list.
     * @return - total appliances power consumption.
     */
    int countPowerConsumption(List<Appliance> appliances);
}
