/***
 * Model.
 */
package com.gabchak.model;

import com.gabchak.user.Appliances.Appliance;

import java.util.List;

/***
 * Class Business Logic implements interface Model.
 */
public class BusinessLogic implements Model {

    /***
     * Domain class instance.
     */
    private Domain domain;

    /***
     * BusinessLogic constructor.
     */
    public BusinessLogic() {
        domain = new Domain();
    }

    /***
     * @return - appliance List.
     */
    @Override
    public final List<Appliance> getApplianceList() {
        return domain.getApplianceList();
    }

    /***
     * Sort appliance List by title.
     */
    @Override
    public final void sortByTitle() {
        domain.sortByTitle();
    }

    /***
     * Sort appliance list by power consumption.
     */
    @Override
    public final void sortByPowerConsumption() {
        domain.sortByPowerConsumption();
    }

    /***
     * @param wattRangeFrom - power consumption range form.
     * @param wattRangeTo - power consumption range to.
     * @return -list of appliance in power consumption range form - to.
     */
    @Override
    public final List<Appliance> appliancesSearch(
            final int wattRangeFrom, final int wattRangeTo) {
        return domain.appliancesSearch(wattRangeFrom, wattRangeTo);
    }

    /***
     * @param list - appliances list.
     * @return - sorted by connected status appliance list.
     */
    @Override
    public final List<Appliance> sortByConnectedAppliance(
            final List<Appliance> list) {
        return domain.sortByConnectedAppliance(list);
    }

    /***
     * @param appliances - appliances list.
     * @return - the total power consumption by connected appliances.
     */
    @Override
    public final int countPowerConsumption(final List<Appliance> appliances) {
        return domain.countPowerConsumption(
                domain.sortByConnectedAppliance(domain.getApplianceList()));
    }
}
