/***
 * Model.
 */
package com.gabchak.model;

import com.gabchak.user.Appliances.Appliance;

import java.util.List;

/***
 *Model interface.
 */
public interface Model {

    /***
     * @return - appliance List.
     */
    List<Appliance> getApplianceList();

    /***
     *Sort appliance List by title.
     */
    void sortByTitle();

    /***
     *Sort appliance list by power consumption.
     */
    void sortByPowerConsumption();

    /***
     *Method return list of appliance in power consumption range form - to.
     * @param wattRangeFrom - power consumption range form.
     * @param wattRangeTo - power consumption range to.
     * @return -list of appliance in power consumption range form - to.
     */
    List<Appliance> appliancesSearch(int wattRangeFrom, int wattRangeTo);

    /***
     *Appliance list is sorted by connected appliance.
     * @param list - appliances list.
     * @return - sorted by connected status appliance list.
     */
    List<Appliance> sortByConnectedAppliance(List<Appliance> list);

    /***
     *Method count appliances power consumption.
     * @param appliances - appliances list.
     * @return - returning the total power consumption by connected appliances.
     */
    int countPowerConsumption(List<Appliance> appliances);


}
