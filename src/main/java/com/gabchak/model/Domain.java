/***
 * Model.
 */
package com.gabchak.model;


import com.gabchak.user.Appliances.*;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/***
 * Class Domain.
 */
public class Domain {

    /***
     * Appliance list.
     */
    private List<Appliance> applianceList;

    /***
     * Domain constructor.
     */
    public Domain() {
        applianceList = new LinkedList<>();
        addAppliancesToList();
    }

    /***
     * The method fills the list with some appliances.
     */
    private void addAppliancesToList() {
        applianceList.add(new TV(140, true));
        applianceList.add(new Fridge(130, true));
        applianceList.add(new Lamp(20, false));
        applianceList.add(new WashingMachine(2000, true));
        applianceList.add(new OtherAppliance("PC", 600, true));
        applianceList.add(new TV(100, false));
        applianceList.add(new Fridge(150, false));
        applianceList.add(new Lamp(10, true));
        applianceList.add(new WashingMachine(2200, false));
        applianceList.add(new OtherAppliance("Monitor", 50, true));
    }

    /***
     * @return - appliance List.
     */
    final List<Appliance> getApplianceList() {
        return applianceList;
    }

    /***
     *Sort appliance List by title.
     */
    final void sortByTitle() {
        applianceList.sort(Comparator.comparing(Appliance::getAppliance));
    }

    /***
     *Sort appliance list by power consumption.
     */
    final void sortByPowerConsumption() {
        applianceList.sort((o1, o2) -> {
            if (o1.getWatts() > o2.getWatts()) {
                return 1;
            } else if (o1.getWatts() < o2.getWatts()) {
                return -1;
            }
            return 0;
        });
    }

    /***
     *Method return list of appliance in power consumption range form - to.
     * @param wattRangeFrom - power consumption range form.
     * @param wattRangeTo - power consumption range to.
     * @return -list of appliance in power consumption range form - to.
     */
    final List<Appliance> appliancesSearch(
            final int wattRangeFrom, final int wattRangeTo) {
        List<Appliance> coincidence = new LinkedList<>();
        for (Appliance appliance : this.applianceList) {
            if (appliance.getWatts() >= wattRangeFrom
                    && appliance.getWatts() <= wattRangeTo) {
                coincidence.add(appliance);
            }
        }
        if (coincidence.isEmpty()) {
            System.out.println("Nothing found in this range.");
        }
        return coincidence;
    }


    /***
     *Appliance list is sorted by connected appliance.
     * @param list - appliances list.
     * @return - sorted by connected status appliance list.
     */
    final List<Appliance> sortByConnectedAppliance(final List<Appliance> list) {
        List<Appliance> coincidence = new LinkedList<>();
        for (Appliance appliance : list) {
            if (appliance.isConnectionStatus()) {
                coincidence.add(appliance);
            }
        }
        return coincidence;
    }

    /***
     *Method count appliances power consumption.
     * @param appliances - appliances list.
     * @return - returning the total power consumption by connected appliances.
     */
    final int countPowerConsumption(final List<Appliance> appliances) {
        int sum = 0;
        for (Appliance appliance : appliances) {
            sum += appliance.getWatts();
        }
        return sum;
    }
}
